import java.lang.reflect.Executable;
import java.util.Objects;
import java.util.Random;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws Exception {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.




//        Урок 9.
//        Checked и unchecked исключения. Иерархия исключений.
//        1.
//        Чем отличаются checked и unchecked исключения?

        System.out.println("!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!");
//        checked значит проверяемые исключения которые нужно оборачивать т.е. обрабатывать либо в try catch либо через throws
//        это все исключения класса Exception
//
//        uncheched это непроверяемые исключения класса RuntimeException и их не нужно или точнее не обязательно специально обрабатывать.


//        2.
//        Создайте проверяемое и непроверяемое исключение

//в данном примере в блоке try проверяемое исключение а в блоке catch не проверяемое

        System.out.println("!!!!!!!!!!!!!!2!!!!!!!!!!!!!!!!!!!!");
//        try {
//            throw new Exception("test");
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//



//        3.
//        Создайте 10 checked и 10 unchecked исключений. Сделайте два массива с ними
        System.out.println("!!!!!!!!!!!!!!!!!!!!!3!!!!!!!!!!!!!!!");
         int n=10;
         Exception[] exceptions=new Exception[n];
         RuntimeException[] runtimeExceptions=new RuntimeException[n];

         for (int i=0;i<exceptions.length;i++){
            exceptions[i]=new Exception("это "+i+" проверяемое исключение");
            runtimeExceptions[i]=new RuntimeException("это "+i+" непроверяемое исключение");
         }


//        4.
//        Сделайте метод, который возвращает из этих массивов случайный элемент;
//        иногда из первого, иногда из второго. Какой тип данных будет возвращать этот
//        метод?

        System.out.println("!!!!!!!!!!!!!!!!!4!!!!!!!!!!!!!!!!!!");
    throw getExc(exceptions,runtimeExceptions);

    //мы указываем то он возвращает Exception, т.к. родителя, т.к. RunException наследуется от него

//                5.
//        Нарисуйте иерархию классов exception и runtime exception до самого начала
   //     System.out.println("!!!!!!!!!!!!!!!!!!!!5!!!!!!!!!!!!!!!1");
   //     Objects->Throwable->Exception

    //    Objects->Throwable->Exception->RuntimeException


//        Критерии оценивания:
//        1 бал
//                л
//        -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнен
//        ы корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов



    }

    public static Exception getExc(Exception[] excArr, RuntimeException[] runExcArr){
        Random random=new Random();
        int n = random.nextInt(10);
        if (n%2==0) {
            return excArr[random.nextInt(10)];
        }
        else return runExcArr[random.nextInt(10)];
    }


}